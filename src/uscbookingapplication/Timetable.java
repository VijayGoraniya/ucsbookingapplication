/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uscbookingapplication;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author vijay
 */
public class Timetable {
    
    private String strClassCode,strClassName;
    private int intClassDay, intNoOfSeats, intClassTime;
    private float fltPrice;

    Map<String, Timetable> mapTimetable = new HashMap<String, Timetable>();
    
    public Timetable()
    {
    }
    public Timetable(String classCode, String className, int classDay,int classTime, int noOfSeats, float price ) {
     
        strClassCode = classCode;
        strClassName = className;
        intClassDay = classDay;
        intClassTime = classTime;
        intNoOfSeats = noOfSeats;
        fltPrice = price;
    }
    
    //To populate timetable when program runs for the first time.
    public void setTimetable()
    {
        Timetable timetableObj1 = new Timetable("C001", "Swimming", 0, 0,4, 80);
        Timetable timetableObj2 = new Timetable("C002", "Yoga", 0, 0, 4, 50);
        Timetable timetableObj3 = new Timetable("C003", "Meditation", 0, 0, 4, 65);
        Timetable timetableObj4 = new Timetable("C004", "Zumba", 0, 0, 4, 40);
        
        mapTimetable.put("C001", timetableObj1);
        mapTimetable.put("C002", timetableObj2);
        mapTimetable.put("C003", timetableObj3);
        mapTimetable.put("C004", timetableObj4);
    }
    
    public  Map<String, Timetable> getTimetableList()
    {
        return mapTimetable;
    }

    /**x
     * @return the strClassCode
     */
    public String getStrClassCode() {
        return strClassCode;
    }

    /**
     * @param strClassCode the strClassCode to set
     */
    public void setStrClassCode(String strClassCode) {
        this.strClassCode = strClassCode;
    }

    /**
     * @return the strClassName
     */
    public String getStrClassName() {
        return strClassName;
    }

    /**
     * @param strClassName the strClassName to set
     */
    public void setStrClassName(String strClassName) {
        this.strClassName = strClassName;
    }

    /**
     * @return the intClassDay
     */
    public int getIntClassDay() {
        return intClassDay;
    }

    /**
     * @param intClassDay the intClassDay to set
     */
    public void setIntClassDay(int intClassDay) {
        this.intClassDay = intClassDay;
    }

    /**
     * @return the intNoOfSeats
     */
    public int getIntNoOfSeats() {
        return intNoOfSeats;
    }

    /**
     * @param intNoOfSeats the intNoOfSeats to set
     */
    public void setIntNoOfSeats(int intNoOfSeats) {
        this.intNoOfSeats = intNoOfSeats;
    }

    /**
     * @return the intClassTime
     */
    public int getIntClassTime() {
        return intClassTime;
    }

    /**
     * @param intClassTime the intClassTime to set
     */
    public void setIntClassTime(int intClassTime) {
        this.intClassTime = intClassTime;
    }

    /**
     * @return the fltPrice
     */
    public float getFltPrice() {
        return fltPrice;
    }

    /**
     * @param fltPrice the fltPrice to set
     */
    public void setFltPrice(float fltPrice) {
        this.fltPrice = fltPrice;
    }
}