/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uscbookingapplication;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import static uscbookingapplication.USCBookingApplication.bookingObj;

/**
 *
 * @author vijay
 */
public class BookClass {
    
    private int bookingId, classWeek, classDay, classTime;
    private String classCode, studentId, bookingStatus; 
    
    Map<Integer, BookClass> mapClassBooking = new HashMap<Integer, BookClass>();
    Scanner scanData = new Scanner(System.in);
    USCBookingApplication applicationObj = new USCBookingApplication();
    
    //Default constructor
    public BookClass()
    {     }
    
    //Parameterisex contructor
    public BookClass(int bookingId, String classCode, int classWeek,int classDay,int classTime, String studentId,String bookingStatus)
    { 
        this.bookingId=bookingId;
        this.classCode=classCode;
        this.classWeek=classWeek;
        this.classDay=classDay;
        this.classTime=classTime;
        this.studentId=studentId;
        this.bookingStatus=bookingStatus;
    }
    
    //To retusn all booking list.
    public  Map<Integer, BookClass> getAllBookingList()
    {
        return mapClassBooking;
    } 
    
    //Add new booking into Map/List.
    public boolean addBooking(int bookingId, String classCode, int classWeek,int classDay,int classTime, String studentId,String bookingStatus)
    {
        try
        {            
         
            BookClass classObj = new BookClass(mapClassBooking.size()+1, classCode, classWeek, classDay, classTime, studentId, bookingStatus);
            mapClassBooking.put(mapClassBooking.size()+1, classObj);
            return true;
        }
        catch(Exception e)
        {
            return false;
        }
    }
    
     //update existing booking into Map/List.
    public void updateBooking(int bookingId, String classCode, int classWeek,int classDay,int classTime, String studentId, String bookingStatus)
    {
        BookClass classObj = new BookClass(bookingId, classCode, classWeek, classDay, classTime, studentId, bookingStatus);
        mapClassBooking.replace(bookingId, classObj);
        
    }
     
        //Function for print monthly class report
    public void monthlyClassReport(int championReportFlag, int monthChoice, Object obj)
    {
        //Feedback feedbackObj = new Feedback();
        //championReport flag will indicates simple report or chamion report request
        //We can store data in array as well, but it will be difficult to retrive through loop
        //To loop in array will occupy memory and time both, so I will store data in local variabls.
        int c001_students = 0, c002_students = 0, c003_students = 0, c004_students = 0;
        int c001_attended = 0, c002_attended = 0, c003_attended = 0, c004_attended = 0;
        float c001_avg_rating = 0, c002_avg_rating = 0, c003_avg_rating = 0, c004_avg_rating = 0;
      
        //Map to retrive and store data
        Map<Integer, Feedback> allFeedbackList = null;
        Map<Integer, BookClass> allBookingList = null;
        
        //To represnt decimal value to two decimal places only
        DecimalFormat formateDecimal = new DecimalFormat("#.##");
        
        if(obj instanceof Feedback)
              allFeedbackList = ((Feedback)obj).getReviewList();
        
        allBookingList = bookingObj.getAllBookingList(); //view all booking functionality  
               
        for(Map.Entry<Integer, BookClass> booking_entry : allBookingList.entrySet())
        {
            //to print the report of current/first month
            if(monthChoice == 1)
            {
                if(booking_entry.getValue().getClassWeek() <= 4 && !booking_entry.getValue().getBookingStatus().equals("Cancelled"))
                {
                    if(booking_entry.getValue().getClassCode().equalsIgnoreCase("c001")){
                        c001_students++;//increase total count of students for class C001
                        if(allFeedbackList.containsKey(booking_entry.getKey()))
                        {
                            c001_avg_rating += allFeedbackList.get(booking_entry.getKey()).getRating();
                            c001_attended++;
                        }
                    }
                    else if(booking_entry.getValue().getClassCode().equalsIgnoreCase("c002")){
                        c002_students++;
                        if(allFeedbackList.containsKey(booking_entry.getKey()))
                        {
                            c002_avg_rating += allFeedbackList.get(booking_entry.getKey()).getRating();
                            c002_attended++;
                        }
                    }
                    else if(booking_entry.getValue().getClassCode().equalsIgnoreCase("c003")){
                        c003_students++;
                        if(allFeedbackList.containsKey(booking_entry.getKey()))
                        {
                            c003_avg_rating += allFeedbackList.get(booking_entry.getKey()).getRating();
                            c003_attended++;
                        }
                    }
                    else{
                        c004_students++;
                        if(allFeedbackList.containsKey(booking_entry.getKey()))
                        {
                            c004_avg_rating += allFeedbackList.get(booking_entry.getKey()).getRating();
                            c004_attended++;
                        }
                    }
                }            
            }
            else
            {
                if(booking_entry.getValue().getClassWeek() > 4)
                {
                    if(booking_entry.getValue().getClassCode().equalsIgnoreCase("c001")){
                        c001_students++;//increase total count of students for class C001
                        if(allFeedbackList.containsKey(booking_entry.getKey()))
                        {
                            c001_avg_rating += allFeedbackList.get(booking_entry.getKey()).getRating();
                            c001_attended++;
                        }
                    }
                    else if(booking_entry.getValue().getClassCode().equalsIgnoreCase("c002")){
                        c002_students++;
                        if(allFeedbackList.containsKey(booking_entry.getKey()))
                        {
                            c002_avg_rating += allFeedbackList.get(booking_entry.getKey()).getRating();
                            c002_attended++;
                        }
                    }
                    else if(booking_entry.getValue().getClassCode().equalsIgnoreCase("c003")){
                        c003_students++;
                        if(allFeedbackList.containsKey(booking_entry.getKey()))
                        {
                            c003_avg_rating += allFeedbackList.get(booking_entry.getKey()).getRating();
                            c003_attended++;
                        }
                    }
                    else{
                        c004_students++;
                        if(allFeedbackList.containsKey(booking_entry.getKey()))
                        {
                            c004_avg_rating += allFeedbackList.get(booking_entry.getKey()).getRating();
                            c004_attended++;
                        }
                    }
                }            
            }
        }
        
        //championReportFlag will indicates which request has been made by user, Normal report or Monthly champion report.
        if(championReportFlag == 0)
        {  
            //FOR MONTHLY REPORT WITH AVERAGE RATING OF EACH CLASS            
            System.out.println(USCBookingApplication.PRINT_BORDER);
            printf("ClassCode");
            printf("ClassName");
            printf("TotalStudents");
            printf("AverageRating");
            System.out.println("\n"+USCBookingApplication.PRINT_BORDER);

            printf("C001");
            printf("Swimming");
            printf(String.valueOf(c001_students));
            printf(String.valueOf((c001_avg_rating > 0) ? formateDecimal.format(c001_avg_rating / c001_attended) : "No rating" ));
            System.out.println("");

            printf("C002");
            printf("Yoga");
            printf(String.valueOf(c002_students));
            printf(String.valueOf((c002_avg_rating > 0) ? formateDecimal.format(c002_avg_rating / c002_attended) : "No rating" ));
            System.out.println("");

            printf("C003");
            printf("Meditation");
            printf(String.valueOf(c003_students));
            printf(String.valueOf((c003_avg_rating > 0) ? formateDecimal.format(c003_avg_rating / c003_attended) : "No rating" ));
            System.out.println("");

            printf("C004");
            printf("Zumba");
            printf(String.valueOf(c004_students));
            printf(String.valueOf((c004_avg_rating > 0) ? formateDecimal.format(c004_avg_rating / c004_attended) : "No rating" ));
        }
        else
        {           
            //FOR MONTHLY CHAMPION REPORT PRINTING
            System.out.println(USCBookingApplication.PRINT_BORDER);
            System.out.println("Champion Class of the month is: ");            
            System.out.println(USCBookingApplication.PRINT_BORDER);
            printf("ClassCode");
            printf("ClassName");
            printf("TotalStudents");
            printf("TotalIncome");
            System.out.println("\n"+USCBookingApplication.PRINT_BORDER);

            float c001_income = c001_students * 80;
            float c002_income = c002_students * 50;
            float c003_income = c003_students * 65;
            float c004_income = c004_students * 40;
            
            if(c001_income > c002_income && c001_income > c003_income && c001_income > c004_income)
            {
                printf("C001");
                printf("Swimming");           
                printf(String.valueOf(c001_students));
                printf(String.valueOf(formateDecimal.format(c001_income)));
            }
            else  if(c002_income > c001_income && c002_income > c003_income && c002_income > c004_income)
            {
                printf("C002");
                printf("Yoga");
                printf(String.valueOf(c002_students));
                printf(String.valueOf(formateDecimal.format(c002_income)));
      
            }
            else  if(c003_income > c001_income && c003_income > c002_income && c003_income > c004_income)
            {
                printf("C003");
                printf("Meditation");
                printf(String.valueOf(c003_students));
                printf(String.valueOf(formateDecimal.format(c003_income)));
          
            }
            else if(c004_income > c001_income && c004_income > c002_income && c004_income > c003_income)  {
                printf("C004");
                printf("Zumba");
                printf(String.valueOf(c004_students));
                printf(String.valueOf(formateDecimal.format(c004_income)));
            } 
            else
                System.out.println("No booking has been done for this month");
        }
        System.out.println("\n"+USCBookingApplication.PRINT_BORDER);
        System.out.println("");
    }
    
    //Special formatting to print timetable in modular formate
    public static void printf(String message)
    {
        System.out.printf("%-15s",message);
    }
    
    /**
     * @return the bookingId
     */
    public int getBookingId() {
        return bookingId;
    }

    /**
     * @param bookingId the bookingId to set
     */
    public void setBookingId(int bookingId) {
        this.bookingId = bookingId;
    }

    /**
     * @return the classWeek
     */
    public int getClassWeek() {
        return classWeek;
    }

    /**
     * @param classWeek the classWeek to set
     */
    public void setClassWeek(int classWeek) {
        this.classWeek = classWeek;
    }

    /**
     * @return the classDay
     */
    public int getClassDay() {
        return classDay;
    }

    /**
     * @param classDay the classDay to set
     */
    public void setClassDay(int classDay) {
        this.classDay = classDay;
    }

    /**
     * @return the classTime
     */
    public int getClassTime() {
        return classTime;
    }

    /**
     * @param classTime the classTime to set
     */
    public void setClassTime(int classTime) {
        this.classTime = classTime;
    }

    /**
     * @return the classCode
     */
    public String getClassCode() {
        return classCode;
    }

    /**
     * @param classCode the classCode to set
     */
    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    /**
     * @return the studentId
     */
    public String getStudentId() {
        return studentId;
    }

    /**
     * @param studentId the studentId to set
     */
    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    /**
     * @return the bookingStatus
     */
    public String getBookingStatus() {
        return bookingStatus;
    }

    /**
     * @param bookingStatus the bookingStatus to set
     */
    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }
    
}
