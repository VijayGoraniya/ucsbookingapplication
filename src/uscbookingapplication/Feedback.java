/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uscbookingapplication;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author vijay
 */
public class Feedback {
    
    private int bookingId;
    private int rating;
    private String feedback;

    private Map<Integer, Feedback> mapFeedback = new HashMap<Integer, Feedback>();
    
    public Feedback()
    {
        //Default constructor
    }
    
    //Parm constructor
    public Feedback(int bookingId, int rating, String feedback)
    {
        this.bookingId = bookingId;
        this.rating  = rating;
        this.feedback = feedback;
    }
    
    //method to add review to the list/map.
    public boolean addReview(int bookingId, int rating, String feedback)
    {
        try
        {
            mapFeedback.put(bookingId, new Feedback(bookingId, rating, feedback));
            return true;
        }
        catch(Exception e)
        {
            return false;
        }
    }
    
    //method toreturn the list/map of all reviews.
    public Map<Integer, Feedback> getReviewList()
    {
        return mapFeedback;
    }
    
    /**
     * @return the bookingId
     */
    public int getBookingId() {
        return bookingId;
    }

    /**
     * @param bookingId the bookingId to set
     */
    public void setBookingId(int bookingId) {
        this.bookingId = bookingId;
    }

    /**
     * @return the rating
     */
    public int getRating() {
        return rating;
    }

    /**
     * @param rating the rating to set
     */
    public void setRating(int rating) {
        this.rating = rating;
    }
    
    /**
     * @return the feedback
     */
    public String getFeedback() {
        return feedback;
    }

    /**
     * @param feedback the feedback to set
     */
    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }
}
