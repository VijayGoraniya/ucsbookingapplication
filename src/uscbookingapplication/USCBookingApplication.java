/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uscbookingapplication;

import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author vijay
 */
public class USCBookingApplication {

    //All possible user choices/ make it final so occupy less memory (better than enum or array).
    final static int BOOK_EXCER=1, CHANGE_BOOKING=2,CANCEL_BOOKING=3,CHK_BOOKING=4, FEEDBACK=5, PRINT_REPORT=6, CHAMPION_CLASS=7, EXIT=8;
    final static int WEEK_1=1,WEEK_2=2,WEEK_3=3,WEEK_4=4,WEEK_5=5,WEEK_6=6,WEEK_7=7,WEEK_8=8;
    final static String SATURDAY="SATURDAY", SUNDAY="SUNDAY", MORNING="MORNING", AFTERNOON="AFTERNOON", EVENING="EVENING", BOOKED_STATUS="Booked", 
                        CANCEL_STATUS="Cancelled", ATTEND_STATUS="Attended",CHANGE_STATUS="Changed", 
                        PRINT_DESH_BORDER="~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~", PRINT_BORDER="===================================================";
        
    static BookClass bookingObj = new BookClass();
    static Timetable timetableObj = new Timetable();
    static Feedback feedbackObj = new Feedback();
    static Scanner scanData = new Scanner(System.in);
    
    //Method to print message in newline on screen
    public static void printNewline(String message)
    {
        System.out.println(message);
    }
    
    //Method to print message on screen
    public static void print(String message)
    {
        System.out.print(message);
    }
   
    //Special formatting to print timetable in modular formate
    public static void printf(String message)
    {
        System.out.printf("%-15s",message);
    }
    
    
    //Method to print menu on screen/console
    public static void printMenu()
    {
        printNewline("");
        printNewline("1. Book Excercise");
        printNewline("2. Change Booking");
        printNewline("3. Cancel Booking");
        printNewline("4. Check My Bookings");            
        printNewline("5. Attend & Review Class");
        printNewline("6. Print Monthly Class Report");
        printNewline("7. Print Monthly Champion Class Report");
        printNewline("8. Exit");
        printNewline("");
    }
    
    //Method for display timetable/check timetable
    public static void checkTimetable()
    {
        int intWeekChoice=0, intDayChoice=0,intTimeChoice=0;   
        String classCodeChoice="", studentId="";
        printNewline("Week"+WEEK_1+"\nWeek"+WEEK_2);
        printNewline("Week"+WEEK_3+"\nWeek"+WEEK_4);
        printNewline("Week"+WEEK_4+"\nWeek"+WEEK_5);
        printNewline("Week"+WEEK_7+"\nWeek"+WEEK_8);
        
        print("\nPlease select week number ["+WEEK_1+", "+WEEK_2+", "+WEEK_3+", "+WEEK_4+", "+WEEK_5+", "+WEEK_6+", "+WEEK_7+", "+WEEK_8+"]: ");
        //take input as week choice
        intWeekChoice = validateInput(scanData.next(), WEEK_1, WEEK_8);
        
        printNewline("\nAvailable Days: [ 1."+SATURDAY+ ", 2."+ SUNDAY+" ]");
        print("\nPlease select Day [ "+1+ ", "+ 2+" ]: ");
        intDayChoice = validateInput(scanData.next(), 1, 2);//Validate input with range of valid only 1 or 2 input
        
        printNewline("\nAvailable Times of Day: [ 1."+MORNING+ ", 2."+ AFTERNOON+", 3."+ EVENING+" ]");
        print("\nPlease select which Time of Day: [ 1, 2, 3 ]: ");
        intTimeChoice = validateInput(scanData.next(), 1, 3);
        
        //Print the whole classes 
        printTimetable();
        
        print("Please Select class by entering class code: ");
        classCodeChoice = validateClassCode(scanData.next());//check for valid input and store into variable
        
        int totalPreviousBooking = checkClassAvailability(intWeekChoice, intDayChoice, intTimeChoice, classCodeChoice);
        if(totalPreviousBooking < 4)
        {
           print("Please enter your student ID: "); 
           studentId = scanData.next();
           
           if(checkDuplicateBooking(intWeekChoice, intDayChoice, intTimeChoice, classCodeChoice, studentId))             
           {
               bookingObj.addBooking(1, classCodeChoice, intWeekChoice, intDayChoice, intTimeChoice, studentId, BOOKED_STATUS);
               printNewline(PRINT_DESH_BORDER);
               printNewline("Class booked successfuly.");
               printNewline("Your Unique Booking ID is:[ "+bookingObj.getAllBookingList().size() +" ]");
               printNewline(PRINT_DESH_BORDER);

           }
           else{
               printNewline(PRINT_DESH_BORDER);
               printNewline("Sorry you have already booked the same class on same time.\n");
               printNewline(PRINT_DESH_BORDER);
           }
        }
        else{
            printNewline(PRINT_DESH_BORDER);
            print("Sorry class is fully booked, try another session or excercise\n");  
            printNewline(PRINT_DESH_BORDER);
        }
    }
    
    //Function to print timetable on consol
    public static void printTimetable()
    {
        //Define and set the timetable 
        Map<String, Timetable> mapTimetable ;
        timetableObj.setTimetable();
        mapTimetable = timetableObj.getTimetableList();
        
        printNewline(PRINT_BORDER);
        printf("ClassCode");
        printf("ClassName");
        printf("ClassPrice");
        printf("TotalCapacity");
        printNewline("\n"+PRINT_BORDER);//Print ==== border line
        
        //Traverse throuhgh the all the excercise classes
        Iterator<Map.Entry<String, Timetable>> itr = mapTimetable.entrySet().iterator();
        while(itr.hasNext())
        {
            Map.Entry<String, Timetable> entry = itr.next(); 
            
            printf(entry.getValue().getStrClassCode());
            printf(entry.getValue().getStrClassName());
            printf(String.valueOf(entry.getValue().getFltPrice()));
            printf(String.valueOf(entry.getValue().getIntNoOfSeats()));
            printNewline("");
        }
        printNewline(PRINT_BORDER);// to print blank line.
    }
    
    
    //Print all the booking of the same student
    public static void checkMyBooking(String bookingStatus)
    {  
        int booking_count = 0;
        String studentId = "";
        print("Please enter your studentID: ");
        studentId = scanData.next();
        
        printNewline(PRINT_BORDER+PRINT_BORDER);
        printf("BookingID");
        printf("ClassCode");
        printf("ClassWeek");
        printf("ClassDay");
        printf("ClassTime");
        printf("StudentID");
        printf("BookingStatus");
        printNewline("\n"+PRINT_BORDER+PRINT_BORDER);
        
        //view all booking functionality
        Map<Integer, BookClass> allBookingList = bookingObj.getAllBookingList();    
        for(Map.Entry<Integer, BookClass> booking_entry:allBookingList.entrySet())
        {  
            if(booking_entry.getValue().getStudentId().equalsIgnoreCase(studentId) && !studentId.isEmpty())
            {
                printf(String.valueOf(booking_entry.getValue().getBookingId()));
                printf(booking_entry.getValue().getClassCode());
                printf("Week_"+String.valueOf(booking_entry.getValue().getClassWeek())); 
                
                //To print class day in user friendly way
                switch (booking_entry.getValue().getClassDay()){
                     case 1: 
                        printf(SATURDAY); 
                        break;
                    default:
                        printf(SUNDAY);
                        break;
                }
                //To print time
                switch (booking_entry.getValue().getClassTime()) {
                    case 1:
                        printf(MORNING);
                        break;
                    case 2:
                        printf(AFTERNOON);
                        break;
                    default:
                        printf(EVENING);
                        break;
                }
                printf(String.valueOf(booking_entry.getValue().getStudentId()));
                printf(String.valueOf(booking_entry.getValue().getBookingStatus()));
                printNewline(""); 
                booking_count++;//indicate that booking is found/available.
            }        
        }    
        
        //Indicates booking found or not
        if(booking_count > 0)
        {
            if(!bookingStatus.isEmpty())
            {
                //Booking cancel functionality
                printNewline("To "+bookingStatus+" the booking, Please enter BookingID: ");
                int b_id = validateInput(scanData.next(), 1, 99999);
                if(checkBookingId(b_id, studentId)){
                    
                    switch (bookingStatus){
                        
                        case ATTEND_STATUS:
                            updateBooking(studentId, b_id, bookingStatus);//Call CancelBooking function with bookingId and StudentID 
                            reviewClass(studentId, b_id);//give rating to the attended class.
                            break;
                        case CANCEL_STATUS: 
                            //for cancelling a existing booking
                            updateBooking(studentId, b_id, bookingStatus);
                            break;
                        default:
                            //For CHANGE hte existing booking 
                            
                            //Proced further for new booking.
                            printNewline("\n****Proceed further to Book new class****");
                            changeBooking(studentId, b_id, bookingStatus);
                            break;
                    }                  
                }                 
                else{
                    printNewline(PRINT_DESH_BORDER);
                    printNewline("Booking not found or already cancelled/attended, Please try again");
                    printNewline(PRINT_DESH_BORDER);
                }
            }
        }
        else
        {
            printNewline(PRINT_DESH_BORDER+"\nSorry, Booking not found\n"+PRINT_DESH_BORDER);
        }
        printNewline(PRINT_BORDER+PRINT_BORDER);// to print blank line.
    }
    
    //Function for attend and give feedback to class and review it
    public static void reviewClass(String studentId, int bookingId)
    {
        int rating = 0;
        String comment = "";
        printNewline("Provide a numerical rating of the class ranging from 1 to 5.");
        printNewline("[1: Very dissatisfied, 2: Dissatisfied, 3: Ok, 4: Satisfied, 5: Very Satisfied]");
        
        rating = validateInput(scanData.next(), 1, 5);//validate user input and store into variable
        
        printNewline("Please write any feedback or comment for class [Write NO if nothing]: ");
        comment = scanData.next();
        
        //add review into list
        feedbackObj.addReview(bookingId, rating, comment);
        printNewline(PRINT_DESH_BORDER);
        printNewline("Feedback submitted successfully.");
        printNewline(PRINT_DESH_BORDER);
    }      
    
    //Function to check weither class is available or not.
    public static int checkClassAvailability(int intWeekChoice, int intDayChoice, int intTimeChoice, String classCode)
    {
        int totalBooking = 0;
        Map<Integer, BookClass> allBookingList = bookingObj.getAllBookingList();    
        
        //traverse all the values
       	for(Map.Entry<Integer, BookClass> booking_entry:allBookingList.entrySet())
        { 
            int bookingId = booking_entry.getKey();
            String classId = booking_entry.getValue().getClassCode();
            int week = booking_entry.getValue().getClassWeek();
            int day = booking_entry.getValue().getClassDay();
            int time = booking_entry.getValue().getClassTime();
            String booking_status = booking_entry.getValue().getBookingStatus();
            
            //Check if the class booking already available or not.
            if(classId.equalsIgnoreCase(classCode))
            {
                if(intWeekChoice == week && intDayChoice == day && intTimeChoice == time && booking_status.equalsIgnoreCase(BOOKED_STATUS))
                {
                    //total count of classes at the same time.
                    totalBooking++;
                }
            }
        }        
        return totalBooking;
    }
    
    //To check the duplicate booking by the same student on the same class and time
    public static boolean checkDuplicateBooking(int intWeekChoice, int intDayChoice, int intTimeChoice, String classCode, String studentId)
    {        
        Map<Integer, BookClass> allBookingList = bookingObj.getAllBookingList();    
        
        //traverse all the values
       	for(Map.Entry<Integer, BookClass> booking_entry:allBookingList.entrySet())
        { 
            int bookingId = booking_entry.getKey();
            String classId = booking_entry.getValue().getClassCode();
            int week = booking_entry.getValue().getClassWeek();
            int day = booking_entry.getValue().getClassDay();
            int time = booking_entry.getValue().getClassTime();
            String s_id = booking_entry.getValue().getStudentId();
            String bookingStatus = booking_entry.getValue().getBookingStatus();
            //Check if the class booking already available or not.
            if(classId.equalsIgnoreCase(classCode))
            {
                if(intWeekChoice == week && intDayChoice == day && intTimeChoice == time && studentId.equalsIgnoreCase(s_id) && (bookingStatus.equalsIgnoreCase(BOOKED_STATUS)
                        || bookingStatus.equalsIgnoreCase(CHANGE_STATUS)) )
                {
                    return false;
                }
            }
        }        
        return true;
    }
    
    //Function for cancel or attend excercise booking
    public static boolean updateBooking(String studentId, int bookingId, String bookingStatus)
    {   
        try
        {
            Map<Integer, BookClass> allBookingList = bookingObj.getAllBookingList();  
            String classCode = allBookingList.get(bookingId).getClassCode();
            int week = allBookingList.get(bookingId).getClassWeek();
            int day = allBookingList.get(bookingId).getClassDay();
            int time = allBookingList.get(bookingId).getClassTime();

            //Update booking
            bookingObj.updateBooking(bookingId, classCode, week, day, time, studentId, bookingStatus);
            printNewline(PRINT_DESH_BORDER);
            printNewline("Booking "+ bookingStatus +" successfuly.");
            printNewline(PRINT_DESH_BORDER);
            return true;//return true if everything is true.
        }
        catch(Exception e)
        {
            return false;
        }        
    }
    
    //Function to change the existing booking
    public static void changeBooking(String studentId, int bookingId, String bookingStatus)
    {
        int intWeekChoice=0, intDayChoice=0,intTimeChoice=0;   
        String classCodeChoice="";
    
        print("\nPlease select week number ["+WEEK_1+", "+WEEK_2+", "+WEEK_3+", "+WEEK_4+", "+WEEK_5+", "+WEEK_6+", "+WEEK_7+", "+WEEK_8+"]: ");
        //take input as week choice
        intWeekChoice = validateInput(scanData.next(), WEEK_1, WEEK_8);
        
        printNewline("\nAvailable Days: [ 1."+SATURDAY+ ", 2."+ SUNDAY+" ]");
        print("\nPlease select Day [ "+1+ ", "+ 2+" ]: ");
        intDayChoice = validateInput(scanData.next(), 1, 2);//Validate input with range of valid only 1 or 2 input
        
        printNewline("\nAvailable Times of Day: [ 1."+MORNING+ ", 2."+ AFTERNOON+", 3."+ EVENING+" ]");
        print("\nPlease select which Time of Day: [ 1, 2, 3 ]: ");
        intTimeChoice = validateInput(scanData.next(), 1, 3);
        
        //Print the whole classes 
        printTimetable();
        
        print("Please Select class by entering class code: ");
        classCodeChoice = validateClassCode(scanData.next());//check for valid input and store into variable
        
        int totalPreviousBooking = checkClassAvailability(intWeekChoice, intDayChoice, intTimeChoice, classCodeChoice);
        if(totalPreviousBooking < 4)
        {
           if(checkDuplicateBooking(intWeekChoice, intDayChoice, intTimeChoice, classCodeChoice, studentId))             
           {
               bookingObj.updateBooking(bookingId, classCodeChoice, intWeekChoice, intDayChoice, intTimeChoice, studentId, bookingStatus);
               printNewline(PRINT_DESH_BORDER);
               printNewline("Class "+bookingStatus+" successfuly.");
               printNewline("Your Unique Booking ID is:[ "+bookingId +" ]");
               printNewline(PRINT_DESH_BORDER);
           }
           else
               printNewline("Sorry you have already booked the same class on same time.\n");
        }
        else{
            printNewline(PRINT_DESH_BORDER);
            print("Sorry class is fully booked, try another session or excercise");  
            printNewline(PRINT_DESH_BORDER);
        }
    }
    
    //Checks user Input is Valid or not.
    public static int validateInput(String Input, int minRange, int maxRange)
    {
        int Number=0;
        try
        {
                Number=Integer.parseInt(Input);  //Return false if Input is not integer and can not convert it into integer.
                if(Number<=0)
                {
                    printNewline("Negative Value or 0 Not Allowed.\nEnter Valid Input : ");
                    Input=null;
                    Input=new Scanner(System.in).nextLine();
                    return validateInput(Input, minRange, maxRange);
                } 
                else if(Number< minRange || Number > maxRange) 
                {
                        printNewline("Invalid Number Range, Should be between "+ minRange +" AND "+maxRange+" \nEnter Valid Number : ");
                        Input=null;
                        Input=new Scanner(System.in).nextLine();
                        return validateInput(Input, minRange, maxRange);
                }
                return Number;
            }
            catch(Exception e)
            {
                    Input=null;
                    printNewline("String or Special Symbols Not Allowed \nPlease Enter Valid Input :");
                    Input=new Scanner(System.in).nextLine();
                    return validateInput(Input, minRange, maxRange); // Calling function Recursively if Input is Not Valid.

            }	
    }

    //Function to check the availablity of booking ID
    public static boolean checkBookingId(int bookingId, String student_id){
        //check weither bookingId available or not, also with the booked sttaus, we don't consider cancelled ones.
        if(bookingObj.getAllBookingList().get(bookingId) == null || bookingObj.getAllBookingList().get(bookingId).getBookingStatus().equalsIgnoreCase(CANCEL_STATUS) ||
           bookingObj.getAllBookingList().get(bookingId).getBookingStatus().equalsIgnoreCase(ATTEND_STATUS) || !bookingObj.getAllBookingList().get(bookingId).getStudentId().equals(student_id))
            return false; //Booking ID not found or not associated with the current student
        
        return true;
    }
    
    //Function to check valid class code or not.
    public static String validateClassCode(String code)
    {
        //only 4 valid class codes
        if(code.equalsIgnoreCase("C001") || code.equalsIgnoreCase("C002") || code.equalsIgnoreCase("C003") || code.equalsIgnoreCase("C004"))
        {
            return code;
        }
        else
        {
            printNewline("Invalid Class Code \nEnter Valid Class code : ");
            code = null;
            code = new Scanner(System.in).nextLine();
            return validateClassCode(code);
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        int userChoice = 0;          
        try
        {
            printNewline("*************Welcome to University Sports Club*************\n");
            //Travers till user make exit choice 
            while(userChoice != EXIT)
            {
                //Display menu with user choice options
                printMenu();
                print("Please Enter Your Choice: ");
                //Validate user input
                userChoice = validateInput(scanData.next(),1,8);    //store user choice into variable
                
                switch(userChoice)
                {
                    case BOOK_EXCER:
                        checkTimetable();
                        break;
                    case CHANGE_BOOKING:
                        //first param indicates cancel booking ope, second review booking, 3rd change booking
                        checkMyBooking(CHANGE_STATUS);//1 indicates booking cancel operation
                        break;
                    case CANCEL_BOOKING:
                        //first param indicates cancel booking ope, second review booking
                        checkMyBooking(CANCEL_STATUS);//1 indicates booking cancel operation
                        break;
                    case CHK_BOOKING:
                        checkMyBooking("");
                        break;
                    case FEEDBACK:
                        checkMyBooking(ATTEND_STATUS);
                        break;
                    case PRINT_REPORT: 
                        printNewline("Please enter which month you want to print report\n[1. First month(1-4 weeks), 2. Second month(5-8 weeks)]");
                        printNewline("Select month [1, 2]: ");
                        int month_choice = validateInput(scanData.next(), 1, 2);
                        //0 parameter indicates that simple report needs to be print
                        bookingObj.monthlyClassReport(0, month_choice, feedbackObj);
                        break;
                    case CHAMPION_CLASS:
                        printNewline("Please enter which month you want to print report\n[1. First month(1-4 weeks), 2. Second month(5-8 weeks)]");
                        printNewline("Select month [1, 2]: ");
                        int month_choice2 = validateInput(scanData.next(), 1, 2);
                        //1 parameter indicates that champion report needs to be print
                        bookingObj.monthlyClassReport(1, month_choice2, feedbackObj);
                        break;
                    case EXIT:
                        printNewline(PRINT_DESH_BORDER);
                        printNewline("Exit, Thank you! Have a great day.");
                        printNewline(PRINT_DESH_BORDER);
                        break;
                }                
            }
        }
        catch(Exception excep)
        {
            printNewline(excep.getMessage());  
        }
        finally
        {
            //close all objects and instances 
            scanData.close();
        }        
    }    
}