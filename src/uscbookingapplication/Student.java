/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uscbookingapplication;

/**
 *
 * @author vijay
 */
public class Student {
    
    private String strStudentName, strCourse;
    private int intStudentId;

    /**
     * @return the strStudentName
     */
    public String getStrStudentName() {
        return strStudentName;
    }

    /**
     * @param strStudentName the strStudentName to set
     */
    public void setStrStudentName(String strStudentName) {
        this.strStudentName = strStudentName;
    }

    /**
     * @return the strCourse
     */
    public String getStrCourse() {
        return strCourse;
    }

    /**
     * @param strCourse the strCourse to set
     */
    public void setStrCourse(String strCourse) {
        this.strCourse = strCourse;
    }

    /**
     * @return the intStudentId
     */
    public int getIntStudentId() {
        return intStudentId;
    }

    /**
     * @param intStudentId the intStudentId to set
     */
    public void setIntStudentId(int intStudentId) {
        this.intStudentId = intStudentId;
    }
    
    
    
}
