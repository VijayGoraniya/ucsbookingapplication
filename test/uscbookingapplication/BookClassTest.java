/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uscbookingapplication;

import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author vijay
 */
public class BookClassTest {
    
    public BookClassTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getAllBookingList method, of class BookClass.
     */
    @Test
    public void testGetAllBookingList() {
        System.out.println("getAllBookingList");
        BookClass instance = new BookClass();
        boolean expectedResult = true;
        
        //Object will return Map of all data
        Object result = instance.getAllBookingList();
        
        //It will return true if the object is type of Map (method will return Map so we will test that.
        boolean getType = result instanceof Map;
                        
        //Checking wiether hte return list is the map or not
        assertEquals(expectedResult, getType);
    }

    /**
     * Test of addBooking method, of class BookClass.
     */
    @Test
    public void testAddBooking() {
        System.out.println("addBooking");
        int bookingId = 1;
        String classCode = "c001";
        int classWeek = 1;
        int classDay = 1;
        int classTime = 1;
        String studentId = "10";
        String bookingStatus = "Booked";
        BookClass instance = new BookClass();
        boolean expResult = true;
        
        //this will return true if the booking has been successful.
        boolean result = instance.addBooking(bookingId, classCode, classWeek, classDay, classTime, studentId, bookingStatus);
        assertEquals(expResult, result);
    }

    

}
