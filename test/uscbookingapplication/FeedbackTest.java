/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uscbookingapplication;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author vijay
 */
public class FeedbackTest {
    
    public FeedbackTest() {
    }

    /**
     * Test of addReview method, of class Feedback.
     */
    @Test
    public void testAddReview() {
        System.out.println("addReview");
        int bookingId = 1;
        int rating = 4;
        String feedback = "None";
        Feedback instance = new Feedback();
        boolean expResult = true;
        
        //this will return true if the booking has been successful.
        boolean result = instance.addReview(bookingId, rating, feedback);
        assertEquals(expResult, result);
    }
}
