/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uscbookingapplication;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author vijay
 */
public class USCBookingApplicationTest {
    
    public USCBookingApplicationTest() {
    }
   
    /**
     * Test of checkClassAvailability method, of class USCBookingApplication.
     */
    @Test
    public void testCheckClassAvailability() {
        System.out.println("checkClassAvailability");
        int intWeekChoice = 1;//first week
        int intDayChoice = 2; // Sunday
        int intTimeChoice = 2;// Afternoon
        String classCode = "C002"; //Yoga
        int expResult = -1;  //it will never return -1 if the function works fine/properly (will return 0 if no previous booking.
        
        //It will return total number of booking done at the same time. 0 if no booking found at same time. 
        int result = USCBookingApplication.checkClassAvailability(intWeekChoice, intDayChoice, intTimeChoice, classCode);
       
        //Output never be same if function works fine. only same if error
        assertNotSame(expResult, result);
    }

    /**
     * Test of checkDuplicateBooking method, of class USCBookingApplication.
     */
    @Test
    public void testCheckDuplicateBookingFalse() {
        
        //Initially book a class with no duplicate data
        //testCheckDuplicateBookingTrue for check with duplicate booking
        System.out.println("checkDuplicateBooking with original data");
        int intWeekChoice = 1;//Week 1
        int intDayChoice = 1;//Saturday
        int intTimeChoice = 1;//Morning
        String classCode = "C001";//Swimming
        String studentId = "10";
        boolean expResult = true;
        
        //it will return true if no dulicate class found
        boolean result = USCBookingApplication.checkDuplicateBooking(intWeekChoice, intDayChoice, intTimeChoice, classCode, studentId);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkDuplicateBooking method, of class USCBookingApplication.
     */
    @Test
    public void testCheckDuplicateBookingTrue() {
        System.out.println("checkDuplicateBooking with duplicate data");
        BookClass intanceBookClass = new BookClass();
        //Check with duplicate data
        //testCheckDuplicateBookingTrue for check with duplicate booking
        System.out.println("checkDuplicateBooking");
        int intWeekChoice = 1;//Week 1
        int intDayChoice = 1;//Saturday
        int intTimeChoice = 1;//Morning
        String classCode = "C001";//Swimming
        String studentId = "10";
        boolean expResult = false;
        
        //book class to check if it allow to book duplicate booking or not.
        intanceBookClass.addBooking(1, classCode, intWeekChoice, intDayChoice, intTimeChoice, studentId, "Booked");  
        //will return false. as we have already booked that class with same studentid and time.
        boolean result = USCBookingApplication.checkDuplicateBooking(intWeekChoice, intDayChoice, intTimeChoice, classCode, studentId);  
        //assertEquals(expResult, result);
        assertNotEquals(expResult, result);
    }
    
    /**
     * Test of testClassCode method, of class USCBookingApplication.
     */
    @Test
    public void testClassCodeValidData() {
        System.out.println("Check ClassCode with Valid Data");
        String class_code = "C002";
        String result = USCBookingApplication.validateClassCode("C002");
        assertEquals(class_code, result);
    }
    
    /**
     * Test of testClassCode method, of class USCBookingApplication.
     */
    @Test
    public void testClassCodeInvalidData() {
        System.out.println("Check ClassCode with Invalid Data");
        String class_code = "ABC";//Test with wrong data it will return false
        String result = USCBookingApplication.validateClassCode("C002");
        assertNotEquals(class_code, result);
    }
    
    /**
     * Test of testUserInput method, of class USCBookingApplication.
     */
    @Test
    public void testUserInput() {
        System.out.println("testUserInput");
        String week_number = "2";//Test with wrong data it will return false
        
        //it will test the integer inputs and return the int if valid integer
        int result = USCBookingApplication.validateInput(week_number, 1, 2);
        assertEquals(2, result);
    }    
}
