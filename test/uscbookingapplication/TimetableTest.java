/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uscbookingapplication;

import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author vijay
 */
public class TimetableTest {
    
    public TimetableTest() {
    }
    
    /**
     * Test of getTimetableList method, of class Timetable.
     */
    @Test
    public void testGetTimetableList() {
        System.out.println("getTimetableList");
        Timetable instance = new Timetable();
        instance.setTimetable();        
        int expResult = 4;
        Map<String, Timetable> result = instance.getTimetableList();
        
        //I have seet 4 excersise classes so compare the size of the list.
        assertEquals(expResult, result.size());
    }
}